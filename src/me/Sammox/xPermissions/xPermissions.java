package me.Sammox.xPermissions;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class xPermissions extends JavaPlugin{

    static HashMap<String, PermissionAttachment> permp = new HashMap<>();

    private xPermissions plugin;

    public void onEnable(){
        System.out.println("[xPermissions] Plugin activated");
        registerCommands();
        registerEvents();
        setupFiles();
    }

    public void onDisable(){
        System.out.println("[xPermissions] Plugin deactivated");
    }

    public void registerCommands(){
        COMMAND_xperm exe1 = new COMMAND_xperm(this);
        getCommand("xperm").setExecutor(exe1);
    }

    public void registerEvents(){
        new LISTENER_AsyncPlayerChat(this);
        new LISTENER_PlayerJoin(this);
        new LISTENER_PlayerQuit(this);
    }


    public void setupFiles(){

        //GROUPS
        File userg = new File("plugins/xPermissions/groups", "user.yml");
        FileConfiguration cfgug = YamlConfiguration.loadConfiguration(userg);
        List<String> permissions = new ArrayList<>();
        permissions.add("test.test");
        List<String> containsgroup = new ArrayList<>();

        if (!userg.exists()) {

            cfgug.set("group.name", "User");
            cfgug.set("group.prefix", "&7");
            cfgug.set("group.default", true);
            cfgug.set("group.contains", containsgroup);
            cfgug.set("group.permissions", permissions);

            try {
                cfgug.save(userg);
                System.out.println("[xPermissions] user.yml created");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            System.out.println("[xPermissions] user.yml loaded");
        }

        //CONFIG
        File config = new File("plugins/xPermissions", "config.yml");
        FileConfiguration cfgc = YamlConfiguration.loadConfiguration(config);

        if(!config.exists()){

            cfgc.set("xpermissions.chatformat", "&3» &7");

            try{
                cfgc.save(config);
                System.out.println("[xPermissions] config.yml created");
            }catch (Exception ex){
                ex.printStackTrace();
            }

        }else{
            System.out.println("[xPermissions] config.yml loaded");
        }

        //MESSAGES
        File msg = new File("plugins/xPermissions", "messages.yml");
        FileConfiguration cfgmsg = YamlConfiguration.loadConfiguration(msg);

        if(!msg.exists()){

            cfgmsg.set("xPerm.prefix", "");

            try{
                cfgmsg.save(msg);
                System.out.println("[xPermissions] messages.yml created");
            }catch (Exception ex){
                ex.printStackTrace();
            }

        }else{
            System.out.println("[xPermissions] messages.yml loaded");
        }

    }

    public static String getDefaultGroup(){
            File userg = new File("plugins/xPermissions/groups");

            for (File file : userg.listFiles()) {
                FileConfiguration cfgg = YamlConfiguration.loadConfiguration(file);

                if (cfgg.get("group.default").equals(true)) {
                    String groupname = cfgg.getString("group.name");
                    return groupname;
                }
            }

        return null;
    }

    public static boolean checkIfGroupExists(String groupname) {
            File userg = new File("plugins/xPermissions/groups");

            for (File file : userg.listFiles()) {

                String filename = file.getName().substring(0, (file.getName().length() - 4));
                if (filename.equalsIgnoreCase(groupname)) {
                    return true;
                }

            }

        return false;
    }

}
