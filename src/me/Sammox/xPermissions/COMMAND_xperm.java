package me.Sammox.xPermissions;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class COMMAND_xperm implements CommandExecutor{

    private xPermissions plugin;

    public COMMAND_xperm(xPermissions instance) {
        this.plugin = instance;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if(sender instanceof Player){

            Player p = (Player) sender;
            File config = new File("plugins/xPermissions", "config.yml");
            FileConfiguration cfg = YamlConfiguration.loadConfiguration(config);

            String Prefix = "§cxPermissions §7» §r";
            String Prefixs = "§c» §r";

            if(args.length == 1){
                if(args[0].equalsIgnoreCase("help")){
                    p.sendMessage(Prefixs + "§cxPermissions Help");
                    p.sendMessage(Prefixs + "§7/§cxperm help");
                    p.sendMessage(Prefixs + "§7/§cxperm setgroup §7<§cname§7> <§cgroup§7>");
                    p.sendMessage(Prefixs + "§7/§cxperm creategroup §7<§cgroup§7>");
                    p.sendMessage(Prefixs + "§7/§cxperm delgroup §7<§cgroup§7>");
                }else{
                    p.sendMessage(Prefix + "§4Please use /xperm help");
                }
            }else if (args.length == 2){

                if(args[0].equalsIgnoreCase("creategroup")){

                    if(p.hasPermission("xperm.creategroup") || p.hasPermission("xperm.*") || p.isOp()){

                        if(xPermissions.checkIfGroupExists(args[1])) {
                            p.sendMessage(Prefix + "§4This group already exists");
                        }else{
                            File group = new File("plugins/xPermissions/groups", args[1] + ".yml");
                            FileConfiguration cfgg = YamlConfiguration.loadConfiguration(group);
                            List<String> permissions = new ArrayList<>();
                            permissions.add("test.test");
                            List<String> containsgroup = new ArrayList<>();

                            cfgg.set("group.name", args[1]);
                            cfgg.set("group.prefix", "&7");
                            cfgg.set("group.default", false);
                            cfgg.set("group.contains", containsgroup);
                            cfgg.set("group.permissions", permissions);

                            try{
                                cfgg.save(group);
                                p.sendMessage(Prefix + "§aGroup successfully created");
                            }catch (Exception ex){
                                ex.printStackTrace();
                            }

                        }

                    }else{
                        p.sendMessage(Prefix + "§4No permissions");
                    }

                }else if(args[0].equalsIgnoreCase("delgroup")) {

                    if(p.hasPermission("xperm.delgroup") || p.hasPermission("xperm.*") || p.isOp()){

                        if(xPermissions.checkIfGroupExists(args[1])){
                            File group = new File("plugins/xPermissions/groups", args[1] + ".yml");
                            FileConfiguration cfgg = YamlConfiguration.loadConfiguration(group);

                            group.delete();

                        }else{
                            p.sendMessage(Prefix + "§4This group not exists");
                        }

                    }else{
                        p.sendMessage(Prefix + "§4No permissions");
                    }

                }else{
                    p.sendMessage(Prefix + "§4Please use /xperm help");
                }

            }else if(args.length == 3) {
                if(args[0].equalsIgnoreCase("setgroup")) {

                    if (p.hasPermission("xperm.setgroup") || p.hasPermission("xperm.*") ||p.isOp()){

                        Player ptarget2 = Bukkit.getPlayer(args[1]);

                        if (!(ptarget2 == null)) {
                            File user = new File("plugins/xPermissions/users", ptarget2.getUniqueId().toString() + ".yml");
                            FileConfiguration cfgu = YamlConfiguration.loadConfiguration(user);

                            if (user.exists()) {
                                File userg = new File("plugins/xPermissions/groups", args[2] + ".yml");

                                if (userg.exists()) {
                                    cfgu.set("user.group", args[2]);

                                    try {
                                        cfgu.save(user);
                                        p.sendMessage(Prefix + "§aThe group of §7" + args[1] + " §asuccessfully changed to §7" + args[2]);

                                        Player ptarget = Bukkit.getPlayer(args[1]);

                                        if (!(ptarget == null)) {
                                            ptarget.kickPlayer(Prefix + "§7Your group has been changed. Please join again.");
                                        }

                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                } else {
                                    p.sendMessage(Prefix + "§4This group is not existing");
                                }
                            } else {
                                p.sendMessage(Prefix + "§4The player needs to be joined once!");
                            }

                        } else {
                            p.sendMessage(Prefix + "§4The player has to be online!");
                        }

                    }else{
                        p.sendMessage(Prefix + "§4No permissions");
                    }

                }else{
                    p.sendMessage(Prefix + "§4Please use /xperm help");
                }
            }else{
                p.sendMessage(Prefix + "§4Please use /xperm help");
            }

        }else{
            sender.sendMessage("[xPermissions] This commmand was designed for using by players only");
        }

        return true;
    }
}
