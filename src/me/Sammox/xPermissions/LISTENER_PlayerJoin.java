package me.Sammox.xPermissions;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.permissions.PermissionAttachment;
import static me.Sammox.xPermissions.xPermissions.permp;
import java.io.File;
import java.util.List;

public class LISTENER_PlayerJoin implements Listener{

    private xPermissions plugin;

    public LISTENER_PlayerJoin(xPermissions main){
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e){
        Player p = e.getPlayer();

        String uuid = p.getUniqueId().toString();
        File user = new File("plugins/xPermissions/users", uuid + ".yml");
        FileConfiguration cfguser = YamlConfiguration.loadConfiguration(user);

        if(!user.exists()){

            cfguser.set("user.name", p.getName());
            cfguser.set("user.uuid", p.getUniqueId().toString());
            cfguser.set("user.group", xPermissions.getDefaultGroup());

            try{
                cfguser.save(user);
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }

        givePerms(p.getUniqueId().toString(), p.getPlayer());
        setNameColor(p.getUniqueId().toString(), p);
        checkForNameChange(p.getUniqueId().toString(), p);

    }

    public void givePerms(String uuid, Player ptarget) {
        File user = new File("plugins/xPermissions/users", uuid + ".yml");
        FileConfiguration cfguser = YamlConfiguration.loadConfiguration(user);

        String groupofplayer = cfguser.getString("user.group");

        File userg = new File("plugins/xPermissions/groups", groupofplayer + ".yml");
        FileConfiguration cfggroup = YamlConfiguration.loadConfiguration(userg);

        List<String> playerperms = cfggroup.getStringList("group.permissions");
        PermissionAttachment attach = ptarget.addAttachment(plugin);
        permp.put(uuid, attach);

        for(String perm : playerperms){
            attach.setPermission(perm, true);
        }

        List<String> containgroups = cfggroup.getStringList("group.contains");

        for(String group : containgroups){
            File groupcontained = new File("plugins/xPermissions/groups", group + ".yml");
            FileConfiguration cfgcontained = YamlConfiguration.loadConfiguration(groupcontained);

            List<String> permofgc = cfgcontained.getStringList("group.permissions");

            for(String perm : permofgc){
                attach.setPermission(perm, true);
            }

        }
    }

    public static void checkForNameChange(String uuid, Player ptarget){
            File user = new File("plugins/xPermissions/users", ptarget.getUniqueId().toString() + ".yml");
            FileConfiguration cfguser = YamlConfiguration.loadConfiguration(user);

            String savedname = cfguser.getString("user.name");
            String currentname = ptarget.getName();

            if (!savedname.equalsIgnoreCase(currentname)) {

                cfguser.set("user.name", currentname);

                try {
                    cfguser.save(user);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
    }

    public static void setNameColor(String uuid, Player ptarget){
            File user = new File("plugins/xPermissions/users", ptarget.getUniqueId().toString() + ".yml");
            FileConfiguration cfguser = YamlConfiguration.loadConfiguration(user);

            String groupofplayer = cfguser.getString("user.group");

            File userg = new File("plugins/xPermissions/groups", groupofplayer + ".yml");
            FileConfiguration cfggroup = YamlConfiguration.loadConfiguration(userg);

            String color = cfggroup.getString("group.prefix");
            String normalname = ptarget.getName();

            ptarget.setDisplayName(ChatColor.translateAlternateColorCodes('&', color) + normalname);
            ptarget.setPlayerListName(ChatColor.translateAlternateColorCodes('&', color) + normalname);
    }

}
