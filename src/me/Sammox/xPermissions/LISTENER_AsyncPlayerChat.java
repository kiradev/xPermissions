package me.Sammox.xPermissions;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.io.File;

public class LISTENER_AsyncPlayerChat implements Listener{

    private xPermissions plugin;

    public LISTENER_AsyncPlayerChat(xPermissions main){
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }

    @EventHandler
    public void onAsyncPlayerChat(AsyncPlayerChatEvent e){
        Player p = e.getPlayer();

        File config = new File("plugins/xPermissions", "config.yml");
        FileConfiguration cfgc = YamlConfiguration.loadConfiguration(config);

        e.setFormat("%s " + ChatColor.translateAlternateColorCodes('&', cfgc.getString("xpermissions.chatformat")) + " %s");
    }
}
