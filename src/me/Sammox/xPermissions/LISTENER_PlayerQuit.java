package me.Sammox.xPermissions;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.permissions.PermissionAttachment;

public class LISTENER_PlayerQuit implements Listener{

    private xPermissions plugin;

    public LISTENER_PlayerQuit(xPermissions main){
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e){
        Player p = e.getPlayer();

        PermissionAttachment attach = xPermissions.permp.get(p.getUniqueId().toString());

        if(attach != null) {
            p.removeAttachment(attach);
        }

    }
}
